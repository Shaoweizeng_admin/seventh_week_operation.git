import requests
import re
from PIL import Image
import os
from io import BytesIO

#分页读取猫眼网电影信息并写入文件
def getmovieinformation(pageIndex):
    url = "http://maoyan.com/board/4?offset={0}".format((pageIndex - 1) * 10)

    header = {
        "Host":"maoyan.com",
        "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Connection":"keep-alive",
        "Accept-Language":"zh-cn",
        "Accept-Encoding":"deflate",
        "Cookie":"_lxsdk_s=164853b8456-74e-cf1-d7b%7C%7C8; __mta=218478502.1531243627928.1531243650074.1531243658602.4; _lxsdk=1A6E888B4A4B29B16FBA1299108DBE9C72410C26A3BDE75317D8905019AECB98; _lxsdk_cuid=164853b8453c8-009561960e114a-491b3407-13c680-164853b8454c8; _csrf=2a24ba721beb77c1a5ddfdcd6bfc1048890c71295ba5b2882abcae3743088301; uuid=1A6E888B4A4B29B16FBA1299108DBE9C72410C26A3BDE75317D8905019AECB98",
        "Upgrade-Insecure-Requests":"1",
        "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.1 Safari/605.1.15",
        "Referer":"http://maoyan.com/board/4?offset=20",
    }

    print("获取第{0}页电影信息...".format(pageIndex))

    res = requests.get(url, headers = header)
    html = res.content.decode("utf-8")

    sequences = re.findall('<i class="board-index .*?">(.*?)</i>', html)
    images = re.findall('<dd>(.|\n)*?<img(.|\n)*?alt(.|\n)*?src="(.*?)"(.|\n)*?</dd>', html)
    names = re.findall('<dd>(.|\n)*?<p(.|\n)*?><a(.|\n)*?>(.*?)</a>', html)
    actors = re.findall('<dd>(.|\n)*?<p class="star">((.|\n)*?)</p>', html)
    showtimes = re.findall('<dd>(.|\n)*?<p class="releasetime">((.|\n)*?)</p>', html)
    scores = re.findall('<dd>(.|\n)*?<p class="score"><i class="integer">(.*?)</i><i class="fraction">(.*?)</i></p>', html)

    #准备写入文件
    f = open('movie.txt', 'a')

    for i in range(len(sequences)):
        print("-"*100)
        print("序号：{0}".format(sequences[i]))
        print("图片：{0}".format(images[i][3].strip()))
        print("电影名称：{0}".format(names[i][3].strip()))
        print(actors[i][1].strip())
        print(showtimes[i][1].strip())
        print("评分：{0}{1}".format(scores[i][1],scores[i][2]))
        print("-"*100)

        #保存图片到文件夹
        if not os.path.exists("moviepics"):
            os.makedirs("moviepics")
        path = os.path.join(os.getcwd(), "moviepics")
        response = requests.get(images[i][3].strip())
        img = Image.open(BytesIO(response.content))
        img.save(path+'/'+names[i][3].strip()+'.jpg')

        #写入文件  
        f.write("-"*100+'\n')
        f.write("序号：{0}".format(sequences[i])+'\n')
        f.write("图片：{0}".format(images[i][3].strip())+'\n')
        f.write("电影名称：{0}".format(names[i][3].strip())+'\n')
        f.write(actors[i][1].strip()+'\n')
        f.write(showtimes[i][1].strip()+'\n')
        f.write("评分：{0}{1}".format(scores[i][1],scores[i][2])+'\n')
        f.write("-"*100+'\n')

    f.close()

#获取排行前100名的电影信息
'''如果文本文件存在，则删除'''
if os.path.exists("movie.txt"):
    os.remove("movie.txt")
    
page = 11
for i in range(1, page):
    getmovieinformation(i)
print("操作完成。")

