import requests
import re

def getHouseInformation(pageIndex):
    url = "http://bj.58.com/dashanzi/chuzu/pn{0}/?PGTID=0d3090a7-01d7-fdbc-7a3d-39c3a2e4c0be&ClickID=1".format(pageIndex)
    #data = { "ClickID":"1" }
    header = {
        "Host":"bj.58.com",
        "Pragma":"no-cache",
        "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Connection":"keep-alive",
        "Accept-Language":"zh-cn",
        "Accept-Encoding":"deflate",
        "Cookie":"f=n; commontopbar_new_city_info=1%7C%E5%8C%97%E4%BA%AC%7Cbj; commontopbar_new_city_info=1%7C%E5%8C%97%E4%BA%AC%7Cbj; f=n; commontopbar_myfeet_tooltip=end; time_create=1533822625451; userid360_xml=63C446875AC3D304A77F667523C62505; commontopbar_ipcity=tj%7C%E5%A4%A9%E6%B4%A5%7C0; 58tj_uuid=8a274f0a-6891-475a-aab4-9a350ba047cf; init_refer=http%253A%252F%252Fcallback.58.com%252Ffirewall%252Fvalid%252F1963598261.do%253Fnamespace%253Dchuzulistphp%2526url%253Ddashanzi.58.com%25252Fchuzu%25252Fpn3%25252F%25253FPGTID%25253D0d309017-01d7-f7ca-7a7b-b1270caa452f%252526ClickID%25253D1; new_session=0; new_uv=2; spm=; utm_source=; 58home=tj; city=tj; xxzl_deviceid=J6ZDN59hwuUWDDlBLtoQeio9aOL2pbwlnPS1GlMwm03xu5Ot1UwEBW4tihfuHyA0; wmda_session_id_2385390625025=1531233039257-4a2727c4-5df7-74c2; f=n; als=0; wmda_new_uuid=1; wmda_uuid=550bdfb0ccc07a79b41619c638998b3b; wmda_visited_projects=%3B2385390625025; id58=c5/njVtEuV2HP1RsBJFFAg==",
        "Upgrade-Insecure-Requests":"1",
        "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.1 Safari/605.1.15",
        "Cache-Control":"no-cache",
    }

    print("获取第{0}页房屋信息...".format(pageIndex))

    res = requests.get(url, headers=header)

    html = res.content.decode("utf-8")

    titles = re.findall('<div class="des">(.|\n)*?<a (.|\n)*?>((.|\n)*?)</a>(.|\n)*?</div>', html)
    images = re.findall('<div class="img_list">(.|\n)*?<img(.|\n)*? lazy_src="(.*?)"(.|\n)*?</div>', html)
    squares = re.findall('<div class="des">(.|\n)*?<p(.|\n)*?>(.*?)&nbsp;&nbsp;&nbsp;&nbsp;(.*?)</p>(.|\n)*?</div>', html)
    moneys = re.findall('<div class="money">(.|\n)*?<b>(.*?)</b>(.*?)</div>', html)

    #print(moneys)

    for i in range(len(titles)):
        print("-"*100)
        print("标题：{0}".format(titles[i][2].strip()))
        print("图片：{0}".format(images[i][2].strip()))
        print("户型：{0} - {1}".format(squares[i][2].strip(),squares[i][3]))
        print("价格：{0} {1}".format(moneys[i][1].strip(),moneys[i][2].strip()))
        print("-"*100)


#获取前五页58租房信息
for i in range(1, 6):
    getHouseInformation(i)