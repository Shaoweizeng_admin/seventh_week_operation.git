from urllib import request, parse
import json


#通过抓取有道翻译获取结果
while True:
    keyword = input("Please input the english word need to be translated: ")
    #回车退出
    if keyword == "": 
        break
    url = "http://fanyi.youdao.com/translate?smartresult=dict&smartresult=rule"

    data = {
        "i":keyword,
        "from":"en",
        "to":"zh-CHS",
        "smartresult":"dict",
        "client":"fanyideskweb",
        "salt":"1531222809603",
        "sign":"4d28008422f4cfaa52eca65ecdb004db",
        "doctype":"json",
        "version":"2.1",
        "keyfrom":"fanyi.web",
        "action":"FY_BY_REALTIME",
        "typoResult":"false",
    }
    data = parse.urlencode(data)
    headers = {
        "Content-Type":"application/x-www-form-urlencoded; charset=UTF-8",
        "Origin":"http://fanyi.youdao.com",
        "Host":"fanyi.youdao.com",
        "Pragma":"no-cache",
        "Accept":"application/json, text/javascript, */*; q=0.01",
        "Cache-Control":"no-cache",
        "Accept-Language":"zh-cn",
        "Accept-Encoding":"deflate",
        "Connection":"keep-alive",
        "Cookie":"___rl__test__cookies=1531222809593; fanyi-ad-closed=1; OUTFOX_SEARCH_USER_ID_NCOO=1498354194.930236; fanyi-ad-id=46607; OUTFOX_SEARCH_USER_ID=2018691294@10.168.1.247; JSESSIONID=aaa-g_qlsNvvs--Do9dsw",
        "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.1 Safari/605.1.15",
        "Referer":"http://fanyi.youdao.com/",
        "Content-Length":len(data),
        "X-Requested-With":"XMLHttpRequest",
    }

    req = request.Request(url,data=bytes(data,encoding="utf-8"),headers=headers)
    res = request.urlopen(req)

    html = res.read().decode("utf-8")

    myjson = json.loads(html)
    result = myjson['translateResult'][0][0]['tgt']

    print("The translation result is: {0}".format(result))
    print("Press Enter to Exit...")


