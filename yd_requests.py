import requests, json

#通过抓取有道翻译获取结果
while True:
    keyword = input("Please input the english word need to be translated: ")
    #回车退出
    if keyword == "": 
        break

    url = "http://fanyi.youdao.com/translate?smartresult=dict&smartresult=rule"

    data = {
        "i":keyword,
        "from":"en",
        "to":"zh-CHS",
        "smartresult":"dict",
        "client":"fanyideskweb",
        "salt":"1531222809603",
        "sign":"4d28008422f4cfaa52eca65ecdb004db",
        "doctype":"json",
        "version":"2.1",
        "keyfrom":"fanyi.web",
        "action":"FY_BY_REALTIME",
        "typoResult":"false",
    }
    res = requests.post(url,data)

    html = res.content.decode("utf-8")
    myjson = json.loads(html)
    
    result = myjson['translateResult'][0][0]['tgt']

    print("The translation result is: {0}".format(result))
    print("Press Enter to Exit...")